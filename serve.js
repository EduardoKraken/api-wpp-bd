// IMPORTAR DEPENDENCIAS
const express    = require("express");
const bodyParser = require("body-parser");
var   cors       = require("cors");
const fileUpload = require("express-fileupload");
const fs         = require("fs");
// IMPORTAR EXPRESS
const app        = express();


const config     = require('./config/index');

//Para servidor con ssl
const server     = require('http').createServer(app);

// Rutas estaticas
app.use('/whatsapp-imagenes',             express.static('./../../whatsapp-imagenes'));

// IMPORTAR PERMISOS
app.use(cors({ origin: '*' }));
// parse requests of content-type: application/json
app.use(bodyParser.json({limit: '5000mb'}));
// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
app.use(fileUpload()); //subir archivos

// ----IMPORTAR RUTAS---------------------------------------->
require("./routes/whatsapp/whatsapp.routes")(app)

/// SERVER LISTEN
server.listen(config.PORT, () => {
    console.log(`Servidor escuchando en el puerto ${config.PORT}`);
});

