const config = require("./index.js");

/********** BASE DE DATOS NUEVO ERP **********/

// PRODUCCION
module.exports = {
  HOST: config.HOST,
  USER: config.USER,
  PASSWORD: config.PASSWORD,
  DB: config.DB
};
