const whatsapp       = require("../../models/operaciones/whatsapp.model");
const fs             = require('fs');
const request        = require("request-promise-native");
const { v4: uuidv4 } = require('uuid')
const axios          = require("axios")
const baseUrl        = "http://127.0.0.1:3004/"

const { exec }       = require('child_process');

exports.whatsSaveMessage = async(req, res) => {
  try {
    
    const { message_create, fullFileName } = req.body

    // Consultar telefono del alumno
    let saldoFavor = await whatsapp.agregarMensajeWhatsApp( message_create, fullFileName ).then( response => response )

    return res.send( saldoFavor )

  } catch (error) {
    console.log( error )
    return res.status(500).send( error )
  }
};

