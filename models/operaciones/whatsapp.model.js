const { result }  = require("lodash");
const sqlERPNUEVO = require("../db.js");

//const constructor
const whatsapp = function(depas) {};

whatsapp.agregarMensajeWhatsApp = ( message_create, fullFileName ) => {

  var u = {
    fromMe                      : message_create._data.id.fromMe,
    remote                      : message_create._data.id.remote,
    mimetype                    : message_create._data.mimetype ? message_create._data.mimetype : '',
    filename                    : message_create._data.filename ? message_create._data.filename : '',
    filehash                    : message_create._data.filehash ? message_create._data.filehash : '',
    body                        : message_create._data.body,
    type                        : message_create._data.type,
    timestamp                   : message_create._data.t,
    notifyName                  : message_create._data.notifyName,
    lastPlaybackProgress        : message_create._data.lastPlaybackProgress,
    isMdHistoryMsg              : message_create._data.isMdHistoryMsg,
    stickerSentTs               : message_create._data.stickerSentTs,
    isAvatar                    : message_create._data.isAvatar,
    requiresDirectConnection    : message_create._data.requiresDirectConnection,
    pttForwardedFeaturesEnabled : message_create._data.pttForwardedFeaturesEnabled,
    isStatusV3                  : message_create._data.isStatusV3,
    mediaKey                    : message_create.mediaKey,
    hasMedia                    : message_create.hasMedia,
    from                        : message_create.from,
    to                          : message_create.to,
    author                      : message_create.author,
    deviceType                  : message_create.deviceType,
    hasQuotedMsg                : message_create.hasQuotedMsg,
    duration                    : message_create.duration,
    location                    : message_create.location,
    isGif                       : message_create.isGif,
    isEphemeral                 : message_create.isEphemeral,
    fullFileName
  }

  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query('INSERT INTO mensajes_whatsapp (`fromMe`, `remote`, `body`, `type`, `timestamp`, `notifyName`, `isMdHistoryMsg`, `stickerSentTs`, `isAvatar`, `requiresDirectConnection`, `pttForwardedFeaturesEnabled`, `isEphemeral`, `isStatusV3`, `mediaKey`, `hasMedia`, `froms`, `tos`, `author`, `deviceType`, `hasQuotedMsg`, `duration`, `location`, `isGif`, `mimetype`, `filename`, `filehash`, `fullFileName` )VALUES( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )', 
    	[ u.fromMe,u.remote,u.body,u.type,u.timestamp,u.notifyName,u.isMdHistoryMsg,u.stickerSentTs,u.isAvatar,u.requiresDirectConnection,u.pttForwardedFeaturesEnabled,u.isEphemeral,u.isStatusV3,u.mediaKey,u.hasMedia,u.from,u.to,u.author,u.deviceType,u.hasQuotedMsg,u.duration,u.location,u.isGif, u.mimetype, u.filename, u.filehash, u.fullFileName ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage, err }); }
      resolve({ id: res.insertId, ...u })
    })
  })
}


whatsapp.existeAlertaDesconeta = (  froms ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM sesiones_whatsapp WHERE whatsapp = ? AND deleted = 0;`,[ froms ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}

whatsapp.addAlertaDesconecta = (  froms ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query('INSERT INTO sesiones_whatsapp ( whatsapp ) VALUES ( ? )', [ froms ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }); }
      resolve({ id: res.insertId, froms })
    })
  })
}

whatsapp.updateAlertaWha = ( from ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE sesiones_whatsapp SET deleted = 1, notificacion = 1  WHERE idsesiones_whatsapp > 0 AND whatsapp = ?;`, [ from ],(err, res) => {
      if (err) { reject(err); return; }
      resolve({ from });
    });
  });
};

module.exports = whatsapp;

