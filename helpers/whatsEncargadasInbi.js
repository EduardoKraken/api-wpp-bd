const { Client, LegacySessionAuth, LocalAuth, MessageMedia } = require('whatsapp-web.js');

const whatsapp             = require("../models/operaciones/whatsapp.model");

const wa     = require("@open-wa/wa-automate");
const qrcode = require('qrcode-terminal');
const fs     = require('fs');
const mime   = require("mime-types");
const hash   = require("hash.js");
const moment = require("moment");
const path   = require("path");
const { open, writeFile } = require("fs").promises;
const { exec }       = require('child_process');


let BASE_IMAGE_PATH = path.resolve(__dirname, "..", "..", "whatsapp-imagenes");

console.log('CARGANDO ENCARGADAS INBI')


/***   ANAHUAC  ***/

const client = new Client({
  authStrategy: new LocalAuth({
    clientId: "encargada-client-two" //Un identificador(Sugiero que no lo modifiques)
  })
})

/***   FRESNOS  ***/

const client6 = new Client({
  authStrategy: new LocalAuth({
    clientId: "encargada-client-seis" //Un identificador(Sugiero que no lo modifiques)
  })
})

/***  SAN MIGUEL***/

const client7 = new Client({
  authStrategy: new LocalAuth({
    clientId: "encargada-client-siete" //Un identificador(Sugiero que no lo modifiques)
  })
})

/***  PABLO LIVA***/

const client9 = new Client({
  authStrategy: new LocalAuth({
    clientId: "encargada-client-nueve" //Un identificador(Sugiero que no lo modifiques)
  })
})

/***   CASA BLANCA  ***/

const client4 = new Client({
  authStrategy: new LocalAuth({
    clientId: "encargada-client-cuatro" //Un identificador(Sugiero que no lo modifiques)
  })
})

/***   INBI ONLINE ***/

const client5 = new Client({
  authStrategy: new LocalAuth({
    clientId: "encargada-client-cinco" //Un identificador(Sugiero que no lo modifiques)
  })
})

/***   MIGUEL ALEMAN  ***/

const client8 = new Client({
  authStrategy: new LocalAuth({
    clientId: "encargada-client-ocho" //Un identificador(Sugiero que no lo modifiques)
  })
})

/***   APODACA CENTRO ***/

const clientAC = new Client({
  authStrategy: new LocalAuth({
    clientId: "encargada-client-apodaca" //Un identificador(Sugiero que no lo modifiques)
  })
})


let sesionesWha = [
  { 
    id              : "encargada-client-two",
    whatsapp        : '5218123404920@c.us', // 5218126033415
    sucursal        : 'ANAHUAC',
    clienteWhatsApp : client
  },

  { 
    id              : "encargada-client-seis",
    whatsapp        : '5218130934877@c.us',
    sucursal        : 'FRESNOS',
    clienteWhatsApp : client6
  },

  { 
    id              : "encargada-client-siete",
    whatsapp        : '5218123405591@c.us',
    sucursal        : 'SAN MIGUEL',
    clienteWhatsApp : client7
  },

  { 
    id              : "encargada-client-nueve",
    whatsapp        : '5218130934874@c.us',
    sucursal        : 'PABLO LIVAS',
    clienteWhatsApp : client9
  },

  { 
    id              : "encargada-client-cuatro",
    whatsapp        : '5218186553151@c.us',
    sucursal        : 'CASA BLANCA',
    clienteWhatsApp : client4
  },

  { 
    id              : "encargada-client-cinco",
    whatsapp        : '5218184027409@c.us',
    sucursal        : 'INBI ONLINE',
    clienteWhatsApp : client5
  },

  { 
    id              : "encargada-client-ocho",
    whatsapp        : '5218123404782@c.us',
    sucursal        : 'MIGUEL ALEMAN',
    clienteWhatsApp : client8
  },

  { 
    id              : "encargada-client-apodaca",
    whatsapp        : '5218121911063@c.us',
    sucursal        : 'APODACA CENTRO',
    clienteWhatsApp : clientAC
  }
]

const enviarWhatsApp = {
  
  async enviarMensajeInbi ( tos, mensaje, imagen, outputFilename, from ) {
    return new Promise(async(resolve, reject) => {
      try{

        let existeSesion = sesionesWha.find( el => el.whatsapp == from )

        if( !existeSesion ){ return reject({ message: 'Teléfono no configurado' }) }

        const { sucursal, clienteWhatsApp } = existeSesion 

        const number_details = await clienteWhatsApp.getNumberId( tos ).then( response => response )

        if (number_details && number_details._serialized ) {

          const fullPath     = path.join(
            path.relative(__dirname, BASE_IMAGE_PATH),
            outputFilename
          );

          const media = MessageMedia.fromFilePath(fullPath);

          let sendMessageData = null

          if(imagen){
            sendMessageData = await clienteWhatsApp.sendMessage( number_details._serialized, media, { caption: "Buen día, le envío su recibo de pago, espero tenga un excelente día" }); // send message
            sendMessageData = await clienteWhatsApp.sendMessage( number_details._serialized, 'Buen día, le envío su recibo de pago, espero tenga un excelente día' ); // send message
          }else{
            sendMessageData = await clienteWhatsApp.sendMessage( number_details._serialized, mensaje ); // send message
          }
          console.log( 'sendMessageData',sendMessageData )
          resolve( sendMessageData )
        } else {
          console.log( 'final_number', number_details )

          reject({ message: tos + "Teléfono no existe"})
        }

      }catch( error ){
        console.log( 'error',error )
        reject({ message: 'Espera 15 segundos, y vuelve a intentarlo, por favor' })
      }
    })
  },

  async getEstatusWhatsApp ( from ) {
    return new Promise(async(resolve, reject) => {
      try{

        let existeSesion = sesionesWha.find( el => el.whatsapp == from )

        if( !existeSesion ){ return reject({ message: 'Teléfono no configurado' }) }

        const { sucursal, clienteWhatsApp } = existeSesion 

        const data = await clienteWhatsApp.getState( ).then( response => response )

        console.log( data )

        resolve( data )

      }catch( error ){
        reject({ message: 'No existe una sesión creada' } )
      }
    })
  },

  async generaQR ( from, generateQR ) {
    return new Promise(async(resolve, reject) => {
      try{

        let existeSesion = sesionesWha.find( el => el.whatsapp == from )

        if( !existeSesion ){ return reject({ message: 'Teléfono no configurado' }) }

        const { sucursal, clienteWhatsApp } = existeSesion 

        console.log( clienteWhatsApp )

        clienteWhatsApp.on('qr', async ( qr ) => {
          console.log(`NECESITA QR: ${sucursal} ENCARGADA`)
          resolve({ message: `QR GENERADO PARA: ${sucursal}`, qr })
        });

        clienteWhatsApp.initialize();

      }catch( error ){
        console.log( error )
        reject({ message: 'No existe una sesión creada', error } )
      }
    })
  },

  async destroySesion ( from ) {
    return new Promise(async(resolve, reject) => {
      try{

        let existeSesion = sesionesWha.find( el => el.whatsapp == from )

        if( !existeSesion ){ return reject({ message: 'Teléfono no configurado' }) }

        const { sucursal, clienteWhatsApp } = existeSesion 

        const data = await clienteWhatsApp.logout().then( response => response );
        console.log( data )

        // si ya existe la alerta de desconectado.... no notificar
        const existeAlertaDesconeta = await whatsapp.existeAlertaDesconeta( from ).then( response => response )

        if( !existeAlertaDesconeta ){
          const addAlertaDesconecta = await whatsapp.addAlertaDesconecta( from ).then( response => response )
        }

        resolve({ message: 'Sesión cerrada, favor de activarla de nuevo' })

      }catch( error ){
        console.log( error )
        reject({ message: 'No existe una sesión creada', error } )
      }
    })
  },

  async initiciarSesion ( from, generateQR ) {
    return new Promise(async(resolve, reject) => {
      try{

        let existeSesion = sesionesWha.find( el => el.whatsapp == from )

        if( !existeSesion ){ return reject({ message: 'Teléfono no configurado' }) }

        const { sucursal, clienteWhatsApp } = existeSesion 

        // Save session values to the file upon successful auth
        clienteWhatsApp.on('authenticated', async (session) => {

          const numero = sesionesWha.find( el => el.id == clienteWhatsApp.options.authStrategy.clientId )

          if( numero ){
            // si ya existe la alerta de desconectado.... no notificar
            const updateAlertaWha = await whatsapp.updateAlertaWha( numero.whatsapp ).then( response => response )
          }

          console.log(`sesion conectada: ${sucursal} ENCARGADA`)

          for( const i in sesionesWha ){
            if( numero && numero.whatsapp == sesionesWha[i].whatsapp ){
              sesionesWha[i].clienteWhatsApp = clienteWhatsApp
            }
          }
          resolve({ message: `sesion conectada: ${sucursal} ENCARGADA`})
        });

        // VALIDAR CUANDO LA SESION SE DESCONECTE
        clienteWhatsApp.on('disconnected', async ( reason ) => {
          try{

            const numero = sesionesWha.find( el => el.id == clienteWhatsApp.options.authStrategy.clientId )

            if( numero ){
              // si ya existe la alerta de desconectado.... no notificar
              const existeAlertaDesconeta = await whatsapp.existeAlertaDesconeta( numero.whatsapp ).then( response => response )

              if( !existeAlertaDesconeta ){
                const addAlertaDesconecta = await whatsapp.addAlertaDesconecta( numero.whatsapp ).then( response => response )
              }
            }

          }catch( error ){
            console.log( error)
          }
        });

        // ESCUCHAR CUANDO SE GENERÉ UN MENSAJE Y SE GUARDA
        clienteWhatsApp.on('message_create', async ( message_create ) => {
          try {

            let permisos = ['chat', 'video', 'image', 'ptt', 'document', 'call_log', 'audio' ]
            
            // VALIDAR SI ES NECESARIO GUARDAR ESTOS DATOS
            if( message_create._data.id.remote != 'status@broadcast' && permisos.includes( message_create._data.type )){

              let fullFileName = ''

              if( message_create._data.mimetype ){
                const fileName     = hash.sha256().update(moment.now().toString()).digest("hex");
                const extension    = mime.extension(message_create._data.mimetype);
                fullFileName       = `${fileName}.${extension}`;
                const fullPath     = path.join(
                  path.relative(__dirname, BASE_IMAGE_PATH),
                  fullFileName
                );
                const mediaData = await wa.decryptMedia(message_create._data);
                try {
                  await writeFile(fullPath, mediaData);
                } catch (error) {
                  console.log(error, 'here 3');
                }
              }

              console.log( message_create._data.type, message_create._data.body)

              let saldoFavor = await whatsapp.agregarMensajeWhatsApp( message_create, fullFileName ).then( response => response )
            }

          } catch (error) {
            exec('pm2 reload all', (error, stdout, stderr) => {
              if (error) {
                console.error(`error: ${error.message}`);
                return;
              }

              if (stderr) {
                console.error(`stderr: ${stderr}`);
                return;
              }

              console.log(`stdout:\n${stdout}`);
            });
          }
        });

        clienteWhatsApp.initialize();

      }catch( error ){
        console.log( error )
        reject({ message: 'No existe una sesión creada', error } )
      }
    })
  },

  async whatChats ( from ) {
    return new Promise(async(resolve, reject) => {
      try{

        let existeSesion = sesionesWha.find( el => el.whatsapp == from )

        if( !existeSesion ){ return reject({ message: 'Teléfono no configurado' }) }

        const { sucursal, clienteWhatsApp } = existeSesion 

        let chat_activos = await clienteWhatsApp.getChats();

        resolve( chat_activos )

      }catch( error ){
        console.log( error )
        reject({ message: 'No existe una sesión creada', error } )
      }
    })
  },
}

for( const i in sesionesWha ){
  const { id, sucursal, clienteWhatsApp, whatsapp } = sesionesWha[i]
  enviarWhatsApp.initiciarSesion( whatsapp, false )
}

module.exports = enviarWhatsApp;

