const { Client, LegacySessionAuth, LocalAuth, MessageMedia } = require('whatsapp-web.js');

const whatsapp             = require("../models/operaciones/whatsapp.model");

const wa     = require("@open-wa/wa-automate");
const qrcode = require('qrcode-terminal');
const fs     = require('fs');
const mime   = require("mime-types");
const hash   = require("hash.js");
const moment = require("moment");
const path   = require("path");
const { open, writeFile } = require("fs").promises;
const { exec }       = require('child_process');


let BASE_IMAGE_PATH = path.resolve(__dirname, "..", "..", "whatsapp-imagenes");

console.log('CARGANDO RECEPCION FAST')


/***   LINDA VISTA  ***/

const clientLV = new Client({
  authStrategy: new LocalAuth({
    clientId: "client-lv" 
  })
})

/***   MITRAS  ***/

const clientMTR = new Client({
  authStrategy: new LocalAuth({
    clientId: "client-mtr" 
  })
})

/***   UNIIIII  ***/

const clientNoria = new Client({
  authStrategy: new LocalAuth({
    clientId: "client-noria" 
  })
})

/***   APODACA  ***/

const clientApodaca = new Client({
  authStrategy: new LocalAuth({
    clientId: "client-apodaca" 
  })
})

/***   ELOY  ***/

const clientEloy = new Client({
  authStrategy: new LocalAuth({
    clientId: "client-eloy-2" 
  })
})

/***   NORIAAAA  ***/

const clientNoriaOficial = new Client({
  authStrategy: new LocalAuth({
    clientId: "client-uni" 
  })
})

/***   ROMULO GARZA  ***/

const clientRomulo = new Client({
  authStrategy: new LocalAuth({
    clientId: "client-romulo" 
  })
})


/***   MARKETING  ***/

const clientMarketing = new Client({
  authStrategy: new LocalAuth({
    clientId: "marketing" 
  })
})

let sesionesWha = [
  { 
    id              : "client-lv",
    whatsapp        : '5218135799502@c.us', // 5218126033415
    sucursal        : 'LINDA VISTA',
    clienteWhatsApp : clientLV
  },

  { 
    id              : "client-mtr",
    whatsapp        : '5218136068399@c.us',
    sucursal        : 'MITRAS',
    clienteWhatsApp : clientMTR
  },

  { 
    id              : "client-noria",
    whatsapp        : '5218120509891@c.us',
    sucursal        : 'UNIII',
    clienteWhatsApp : clientNoria
  },

  { 
    id              : "client-apodaca",
    whatsapp        : '5218120493604@c.us',
    sucursal        : 'APODACA',
    clienteWhatsApp : clientApodaca
  },

  { 
    id              : "client-eloy-2",
    whatsapp        : '5218115074724@c.us',
    sucursal        : 'ELOY',
    clienteWhatsApp : clientEloy
  },

  { 
    id              : "client-uni",
    whatsapp        : '5218119269705@c.us',
    sucursal        : 'NORIAAA',
    clienteWhatsApp : clientNoriaOficial
  },

  { 
    id              : "client-romulo",
    whatsapp        : '5218118768517@c.us',
    sucursal        : 'ROMULOOO',
    clienteWhatsApp : clientRomulo
  },

  { 
    id              : "marketing",
    whatsapp        : '5218120494796@c.us',
    sucursal        : 'MARKETING',
    clienteWhatsApp : clientMarketing
  },

]


const enviarWhatsApp = {
  
  async enviarMensajeInbi ( tos, mensaje, imagen, outputFilename, from ) {
    return new Promise(async(resolve, reject) => {
      try{

        let existeSesion = sesionesWha.find( el => el.whatsapp == from )

        if( !existeSesion ){ return reject({ message: 'Teléfono no configurado' }) }

        const { sucursal, clienteWhatsApp } = existeSesion 

        const number_details = await clienteWhatsApp.getNumberId( tos ).then( response => response )

        if (number_details && number_details._serialized ) {

          const fullPath     = path.join(
            path.relative(__dirname, BASE_IMAGE_PATH),
            outputFilename
          );

          const media = MessageMedia.fromFilePath(fullPath);

          let sendMessageData = null

          if(imagen){
            sendMessageData = await clienteWhatsApp.sendMessage( number_details._serialized, media, { caption: "Buen día, le envío su recibo de pago, espero tenga un excelente día" }); // send message
            sendMessageData = await clienteWhatsApp.sendMessage( number_details._serialized, 'Buen día, le envío su recibo de pago, espero tenga un excelente día' ); // send message
          }else{
            sendMessageData = await clienteWhatsApp.sendMessage( number_details._serialized, mensaje ); // send message
          }
          console.log( 'sendMessageData',sendMessageData )
          resolve( sendMessageData )
        } else {
          console.log( 'final_number', number_details )

          reject({ message: tos + "Teléfono no existe"})
        }

      }catch( error ){
        console.log( 'error',error )
        reject({ message: 'Espera 15 segundos, y vuelve a intentarlo, por favor' })
      }
    })
  },

  async getEstatusWhatsApp ( from ) {
    return new Promise(async(resolve, reject) => {
      try{

        let existeSesion = sesionesWha.find( el => el.whatsapp == from )

        if( !existeSesion ){ return reject({ message: 'Teléfono no configurado' }) }

        const { sucursal, clienteWhatsApp } = existeSesion 

        const data = await clienteWhatsApp.getState( ).then( response => response )

        console.log( data )

        resolve( data )

      }catch( error ){
        reject({ message: 'No existe una sesión creada' } )
      }
    })
  },

  async generaQR ( from, generateQR ) {
    return new Promise(async(resolve, reject) => {
      try{

        let existeSesion = sesionesWha.find( el => el.whatsapp == from )

        if( !existeSesion ){ return reject({ message: 'Teléfono no configurado' }) }

        const { sucursal, clienteWhatsApp } = existeSesion 

        console.log( clienteWhatsApp )

        clienteWhatsApp.on('qr', async ( qr ) => {
          console.log(`NECESITA QR: ${sucursal} ENCARGADA`)
          resolve({ message: `QR GENERADO PARA: ${sucursal}`, qr })
        });

        clienteWhatsApp.initialize();

      }catch( error ){
        console.log( error )
        reject({ message: 'No existe una sesión creada', error } )
      }
    })
  },

  async destroySesion ( from ) {
    return new Promise(async(resolve, reject) => {
      try{

        let existeSesion = sesionesWha.find( el => el.whatsapp == from )

        if( !existeSesion ){ return reject({ message: 'Teléfono no configurado' }) }

        const { sucursal, clienteWhatsApp } = existeSesion 

        const data = await clienteWhatsApp.logout().then( response => response );
        console.log( data )

        // si ya existe la alerta de desconectado.... no notificar
        const existeAlertaDesconeta = await whatsapp.existeAlertaDesconeta( from ).then( response => response )

        if( !existeAlertaDesconeta ){
          const addAlertaDesconecta = await whatsapp.addAlertaDesconecta( from ).then( response => response )
        }

        resolve({ message: 'Sesión cerrada, favor de activarla de nuevo' })

      }catch( error ){
        console.log( error )
        reject({ message: 'No existe una sesión creada', error } )
      }
    })
  },

  async initiciarSesion ( from, generateQR ) {
    return new Promise(async(resolve, reject) => {
      try{

        let existeSesion = sesionesWha.find( el => el.whatsapp == from )

        if( !existeSesion ){ return reject({ message: 'Teléfono no configurado' }) }

        const { sucursal, clienteWhatsApp } = existeSesion 

        // Save session values to the file upon successful auth
        clienteWhatsApp.on('authenticated', async (session) => {

          const numero = sesionesWha.find( el => el.id == clienteWhatsApp.options.authStrategy.clientId )

          if( numero ){
            // si ya existe la alerta de desconectado.... no notificar
            const updateAlertaWha = await whatsapp.updateAlertaWha( numero.whatsapp ).then( response => response )
          }

          console.log(`sesion conectada: ${sucursal} ENCARGADA`)

          for( const i in sesionesWha ){
            if( numero && numero.whatsapp == sesionesWha[i].whatsapp ){
              sesionesWha[i].clienteWhatsApp = clienteWhatsApp
            }
          }
          resolve({ message: `sesion conectada: ${sucursal} ENCARGADA`})
        });

        // VALIDAR CUANDO LA SESION SE DESCONECTE
        clienteWhatsApp.on('disconnected', async ( reason ) => {
          try{

            const numero = sesionesWha.find( el => el.id == clienteWhatsApp.options.authStrategy.clientId )

            if( numero ){
              // si ya existe la alerta de desconectado.... no notificar
              const existeAlertaDesconeta = await whatsapp.existeAlertaDesconeta( numero.whatsapp ).then( response => response )

              if( !existeAlertaDesconeta ){
                const addAlertaDesconecta = await whatsapp.addAlertaDesconecta( numero.whatsapp ).then( response => response )
              }
            }

          }catch( error ){
            console.log( error)
          }
        });

        // ESCUCHAR CUANDO SE GENERÉ UN MENSAJE Y SE GUARDA
        clienteWhatsApp.on('message_create', async ( message_create ) => {
          try {

            let permisos = ['chat', 'video', 'image', 'ptt', 'document', 'call_log', 'audio' ]
            
            // VALIDAR SI ES NECESARIO GUARDAR ESTOS DATOS
            if( message_create._data.id.remote != 'status@broadcast' && permisos.includes( message_create._data.type )){

              let fullFileName = ''

              if( message_create._data.mimetype ){
                const fileName     = hash.sha256().update(moment.now().toString()).digest("hex");
                const extension    = mime.extension(message_create._data.mimetype);
                fullFileName       = `${fileName}.${extension}`;
                const fullPath     = path.join(
                  path.relative(__dirname, BASE_IMAGE_PATH),
                  fullFileName
                );
                const mediaData = await wa.decryptMedia(message_create._data);
                try {
                  await writeFile(fullPath, mediaData);
                } catch (error) {
                  console.log(error, 'here 3');
                }
              }

              console.log( message_create._data.type, message_create._data.body)

              let saldoFavor = await whatsapp.agregarMensajeWhatsApp( message_create, fullFileName ).then( response => response )
            }

          } catch (error) {
            exec('pm2 reload all', (error, stdout, stderr) => {
              if (error) {
                console.error(`error: ${error.message}`);
                return;
              }

              if (stderr) {
                console.error(`stderr: ${stderr}`);
                return;
              }

              console.log(`stdout:\n${stdout}`);
            });
          }
        });

        clienteWhatsApp.initialize();

      }catch( error ){
        console.log( error )
        reject({ message: 'No existe una sesión creada', error } )
      }
    })
  },

  async whatChats ( from ) {
    return new Promise(async(resolve, reject) => {
      try{

        let existeSesion = sesionesWha.find( el => el.whatsapp == from )

        if( !existeSesion ){ return reject({ message: 'Teléfono no configurado' }) }

        const { sucursal, clienteWhatsApp } = existeSesion 

        let chat_activos = await clienteWhatsApp.getChats();

        resolve( chat_activos )

      }catch( error ){
        console.log( error )
        reject({ message: 'No existe una sesión creada', error } )
      }
    })
  },
}

for( const i in sesionesWha ){
  const { id, sucursal, clienteWhatsApp, whatsapp } = sesionesWha[i]
  enviarWhatsApp.initiciarSesion( whatsapp, false )
}

module.exports = enviarWhatsApp;
